import { BrowserRouter, Routes, Route, Navigate, Router } from "react-router-dom";
import NotFoundView from "./components/NotFound";
import Signup from "./views/SignUp";
import Login from "./views/Login";
const RoutesProvider = () => {

  return (
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Signup/>} />
          <Route path="/login" element={<Login/>}/>
          <Route path="*" element={<NotFoundView />} />  
        </Routes>
    </BrowserRouter>
  );
};

export default RoutesProvider;
